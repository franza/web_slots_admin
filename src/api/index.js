import axios from 'axios';

const API_URL = 'https://moxisgames.com/ws_server_temp/v1/test';
// const API_URL = '/v1/test';

const getConfigsList = () => {
  return axios.get(API_URL + '/configs')
    .then(response => response.data);
};

const getConfig = (configId) => {
  return axios.get(API_URL + '/config', { params: { configId } })
    .then(response => response.data.config);
};

const updateConfig = (configId, configText) => {
  return axios.post(API_URL + '/updateConfig', { configId, configText })
    .then(response => response.data.config);
};

const testConfig = (configId, lines, iterations) => {
  return axios.get(API_URL + '/testConfig', { params: { configId, lines, iterations } })
    .then(response => response.data);
};

export default { getConfigsList, testConfig, updateConfig, getConfig };
