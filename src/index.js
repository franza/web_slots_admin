import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import registerServiceWorker from './registerServiceWorker';
import App from './App';

import './index.css';

console.log(`process.env.NODE_ENV = ${process.env.NODE_ENV}`);

const store = configureStore({
  lines: localStorage['lines'] ? parseInt(localStorage.getItem('lines'), 10) : -1,
  iterations: localStorage['iterations'] ? parseInt(localStorage.getItem('iterations'), 10) : -1,
});

// https://github.com/reactjs/redux/tree/master/examples/shopping-cart

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

registerServiceWorker();
