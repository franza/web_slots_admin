import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import CircularProgress from 'material-ui/CircularProgress';
import { RaisedButton } from 'material-ui';
import injectTapEventPlugin from 'react-tap-event-plugin';
import Header from './components/Header/Header';
import ResultPanel from './components/ResultPanel/ResultPanel';
import ChangeConfigDialog from './components/ChangeConfigDialog/ChangeConfigDialog';

import { loadConfigsList } from './actions/configsList';
import { startTest } from './actions/test';
import { getCurrentConfigId } from './reducers';

import './App.css';

injectTapEventPlugin();

class App extends Component {

  componentDidMount() {
    this.props.loadConfigsList();
  }

  onStartTest = () => {
    const configId = this.props.currentSlotId;
    const lines = this.props.lines;
    const iterations = this.props.iterations;

    this.props.startTest(configId, lines, iterations);
  };

  render() {
    return (
      <MuiThemeProvider>
        <div>{
          (this.props.configsIds.length === 0) ?
            <div className="centered-container">
              <CircularProgress className="loading-spinner"/>
            </div>
            :
            <div>
              <Header />

              <div className="centered-container">
                <RaisedButton className="button"
                              label="Start test"
                              primary={true}
                              onTouchTap={this.onStartTest}
                              disabled={this.props.isTesting}
                />
              </div>
              <ResultPanel />

            </div>
        }
          <ChangeConfigDialog />
        </div>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
  configsIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  lines: PropTypes.number.isRequired,
  iterations: PropTypes.number.isRequired,
  isTesting: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => {
  const { configsIds, lines, iterations, isTesting } = state;
  return {
    configsIds,
    lines,
    iterations,
    isTesting,
    currentSlotId: getCurrentConfigId(state)
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadConfigsList: () => dispatch(loadConfigsList()),
    startTest: (configId, lines, iterations) => dispatch(startTest(configId, lines, iterations)),
  };
};

// component-container
export default connect(mapStateToProps, mapDispatchToProps)(App);
