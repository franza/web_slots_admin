import api from '../api';

export const CONFIG_REQUEST = 'CONFIG_REQUEST';
export const configRequest = () => ({ type: CONFIG_REQUEST });

export const CONFIG_RECEIVED = 'CONFIG_RECEIVED';
export const configReceived = (config) => ({ type: CONFIG_RECEIVED, data: config });

export const CONFIG_ERRORED = 'CONFIG_ERRORED';
export const configErrored = (error) => ({ type: CONFIG_ERRORED, data: error });

export const loadConfig = (configId) => (
  (dispatch) => {
    dispatch(configRequest());

    api.getConfig(configId)
      .then(config => dispatch(configReceived(config)))
      .catch(error => dispatch(configErrored(error)));
  }
);

export const START_EDIT_CONFIG = 'START_EDIT_CONFIG';
export const startEditConfig = () => ({ type: START_EDIT_CONFIG });

export const STOP_EDIT_CONFIG = 'STOP_EDIT_CONFIG';
export const stopEditConfig = () => ({ type: STOP_EDIT_CONFIG });

export const CONFIG_SAVED = 'CONFIG_SAVED';
export const configSaved = () => ({ type: CONFIG_SAVED });

export const saveConfig = (configId, configData) =>
  (dispatch) => {
    api.updateConfig(configId, configData)
      .then(config => dispatch(configSaved()))
      .catch(error => dispatch(configErrored(error)));
  };
