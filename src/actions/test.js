import api from '../api';

export const startTest = (configId, lines, iterations) => (
  (dispatch) => {
    dispatch(testStarted());

    api.testConfig(configId, lines, iterations)
      .then(result => dispatch(testCompleted(result)))
      .catch(error => dispatch(testErrored(error)));
  }
);

export const START_TESTING = 'START_TESTING';
export const testStarted = () => ({ type: START_TESTING });

export const TEST_COMPLETED = 'TEST_COMPLETED';
export const testCompleted = (result) => ({ type: TEST_COMPLETED, data: result });

export const TEST_ERRORED = 'TEST_ERRORED';
export const testErrored = (error) => ({ type: TEST_ERRORED, data: error });
