export const SELECT_CONFIG = 'SELECT_CONFIG';
export const selectConfig = (configIndex) => ({ type: SELECT_CONFIG, data: configIndex });

export const CHANGE_LINES = 'CHANGE_LINES';
export const changeLines = (lines) => {
  localStorage.setItem('lines', lines);
  return { type: CHANGE_LINES, data: lines };
};

export const CHANGE_ITERATIONS = 'CHANGE_ITERATIONS';
export const changeIterations = (iterations) => {
  localStorage.setItem('iterations', iterations);
  return { type: CHANGE_ITERATIONS, data: iterations };
};
