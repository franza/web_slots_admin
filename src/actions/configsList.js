import api from '../api';

export const CONFIGS_LIST_REQUEST = 'CONFIGS_LIST_REQUEST';
export const configsListRequest = () => ({ type: CONFIGS_LIST_REQUEST });

export const CONFIGS_LIST_RECEIVED = 'CONFIGS_LIST_RECEIVED';
export const configsListReceived = (list) => ({ type: CONFIGS_LIST_RECEIVED, data: list });

export const CONFIGS_LIST_ERRORED = 'CONFIGS_LIST_ERRORED';
export const configsListErrored = (error) => ({ type: CONFIGS_LIST_ERRORED, data: error });

export const loadConfigsList = () => (
  (dispatch) => {
    dispatch(configsListRequest());

    api.getConfigsList()
      .then(config => dispatch(configsListReceived(config)))
      .catch(error => dispatch(configsListErrored(error)));
  }
);
