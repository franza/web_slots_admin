import * as actions from '../actions/testOptions';

export function selectedConfig(state, action) {
  switch (action.type) {
    case actions.SELECT_CONFIG:
      return action.data;

    default:
      return state || 0;
  }
}

export function lines(state, action) {
  switch (action.type) {
    case actions.CHANGE_LINES:
      return action.data;

    default:
      return state || 0;
  }
}

export function iterations(state, action) {
  switch (action.type) {
    case actions.CHANGE_ITERATIONS:
      return action.data;

    default:
      return state || 0;
  }
}
