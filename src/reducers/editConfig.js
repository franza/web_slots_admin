import * as actions from '../actions/editConfig';

export function config(state, action) {
  switch (action.type) {
    case actions.CONFIG_REQUEST:
      return null;

    case actions.CONFIG_RECEIVED:
      return action.data;

    case actions.CONFIG_ERRORED:
      return null;

    default:
      return state || null;
  }
}

export function isConfigLoading(state, action) {
  switch (action.type) {
    case actions.CONFIG_REQUEST:
      return true;

    case actions.CONFIG_RECEIVED:
      return false;

    default:
      return state || false;
  }
}

export function isConfigEditing(state, action) {
  switch (action.type) {
    case actions.START_EDIT_CONFIG:
      return true;

    case actions.STOP_EDIT_CONFIG:
    case actions.CONFIG_SAVED:
      return false;

    default:
      return state || false;
  }
}
