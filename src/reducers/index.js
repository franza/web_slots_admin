import { combineReducers } from 'redux';
import * as configsList from './configsList';
import * as editConfig from './editConfig';
import * as test from './test';
import * as testOptions from './testOptions';

const rootReducer = combineReducers(
  Object.assign({},
    configsList,
    editConfig,
    test,
    testOptions,
  )
);

// helper for getting selected configId
export const getCurrentConfigId = state => state.configsIds[state.selectedConfig];

export default rootReducer;