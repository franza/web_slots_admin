import * as actions from '../actions/configsList';

export function configsIds(state, action) {
  switch (action.type) {
    case actions.CONFIGS_LIST_RECEIVED:
      return action.data;

    case actions.CONFIGS_LIST_ERRORED:
      return null;

    default:
      return state || [];
  }
}
