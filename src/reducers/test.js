import * as actions from '../actions/test';

export function testResult(state, action) {
  switch (action.type) {
    case actions.TEST_COMPLETED:
      return action.data;

    case actions.START_TESTING:
      return {};

    default:
      return state || {};
  }
}

export function isTesting(state, action) {
  switch (action.type) {
    case actions.START_TESTING:
      return true;

    case actions.TEST_COMPLETED:
    case actions.TEST_ERRORED:
      return false;

    default:
      return state || false;
  }
}
