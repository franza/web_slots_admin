import React from 'react';
import PropTypes from 'prop-types';

const ResultItem = ({ resultKey, resultValue }) => (
  <pre className='result-panel-item'>
    {resultKey} = <b>{resultValue}</b>
  </pre>
);

ResultItem.propTypes = {
  resultKey: PropTypes.string.isRequired,
  resultValue: PropTypes.any.isRequired,
};

export default ResultItem;
