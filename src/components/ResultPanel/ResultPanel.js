import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {CircularProgress, Card, CardTitle, CardText} from 'material-ui';
import ResultItem from './ResultItem';

import './ResultPanel.css';

const resultToArray = (response) => {
  if (!response || Object.keys(response).length === 0) {
    return [];
  }

  return [
    ['Slot id ', response.slotId],
    ['Payback', response.win / response.spend],
    ['Win', response.win.toLocaleString()],
    ['Spend', response.spend.toLocaleString()],
    ['Balance', (response.win < response.spend ? '-' : '+') + (Math.abs(response.win - response.spend).toLocaleString())],
    ['Free spins', response.freeSpins],
    ['Bonus games count', response.bonusGamesCount],
    ['Bonus games', JSON.stringify(response.bonusGames)],
    ['Extra win factors', JSON.stringify(response.extraFactors)],
    ['Test time, ms', response.testTime]
  ];
};

class ResultPanel extends Component {
  render() {
    return (
      <Card>
        <CardTitle title="Result" className="card-title"/>
        {
          this.props.isTesting ?
            <div className="centered-container">
              <CircularProgress className="loading-spinner"/>
            </div>
            :
            <CardText style={{ marginLeft: 20 }} expandable={false}>{
              resultToArray(this.props.testResult).map((value, index) => {
                return <ResultItem key={index} resultKey={value[0]} resultValue={value[1]} />
              })}
            </CardText>
        }
      </Card>
    );
  }
}

ResultPanel.propTypes = {
  configsIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  selectedConfig: PropTypes.number.isRequired,
  testResult: PropTypes.object,
  isTesting: PropTypes.bool,
};

const mapStateToProps = ({ configsIds, selectedConfig, testResult, isTesting }) => {
  return {
    configsIds,
    selectedConfig,
    testResult,
    isTesting,
  };
};

export default connect(mapStateToProps)(ResultPanel);
