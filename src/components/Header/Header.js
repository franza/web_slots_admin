import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { RaisedButton, Card, CardText, CardTitle, SelectField, MenuItem } from 'material-ui';

import { startEditConfig } from '../../actions/editConfig';
import * as actions from '../../actions/testOptions';

import './Header.css';

const linesValues = [1, 2, 3, 4, 5, 6, 7, 8, 9];
const iterationsValues = [1000, 10000, 100000];

const formatPrice = (value) => {
  return value.toLocaleString();
};

class Header extends Component {
  componentDidMount() {
    if (this.props.lines === -1) {
      this.props.changeLines(linesValues[linesValues.length - 1]);
    }

    if (this.props.iterations === -1) {
      this.props.changeIterations(iterationsValues[1]);
    }
  }

  handleChangeConfig = (event, index, value) => {
    this.props.selectConfig(value);
  };

  handleChangeLines = (event, index, value) => {
    this.props.changeLines(value);
  };

  handleChangeIterations = (event, index, value) => {
    this.props.changeIterations(value);
  };

  handleClickEdit = () => {
    this.props.startEditConfig(this.props.configsIds[this.props.selectedConfig]);
  };

  render() {
    return (
      <Card>
        <CardTitle title="Admin panel v1.0.2" className="card-title" />
        <CardText>
          <SelectField style={{ float: "left", marginLeft: 20, width: 150 }}
                       autoWidth={true}
                       floatingLabelText="Select config"
                       value={this.props.selectedConfig}
                       onChange={this.handleChangeConfig}
          >
            {this.props.configsIds.map((name, index) => {
              return <MenuItem key={index} value={index} primaryText={name}/>
            })}
          </SelectField>

          <RaisedButton style={{ float: "left", marginTop: 27 }}
                        label="Edit"
                        secondary={true}
                        onTouchTap={this.handleClickEdit}/>

          <SelectField style={{ float: "left", marginLeft: 50, width: 70 }}
                       floatingLabelText="Lines"
                       value={this.props.lines}
                       onChange={this.handleChangeLines}
          >
            {linesValues.map((value, index) => {
              return <MenuItem key={index} value={value} primaryText={value}/>
            })}
          </SelectField>

          <SelectField style={{ marginLeft: 50, width: 150 }}
                       floatingLabelText="Iterations"
                       value={this.props.iterations}
                       onChange={this.handleChangeIterations}
          >
            {iterationsValues.map((value, index) => {
              return <MenuItem key={index} value={value} primaryText={formatPrice(value)}/>
            })}
          </SelectField>

        </CardText>
      </Card>
    );
  }
}

Header.propTypes = {
  configsIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  selectedConfig: PropTypes.number.isRequired,
  lines: PropTypes.number.isRequired,
  iterations: PropTypes.number.isRequired,
  selectConfig: PropTypes.func.isRequired,
  changeLines: PropTypes.func.isRequired,
  changeIterations: PropTypes.func.isRequired,
  startEditConfig: PropTypes.func.isRequired,
};

const mapStateToProps = ({ configsIds, selectedConfig, lines, iterations }) => {
  return {
    configsIds,
    selectedConfig,
    lines,
    iterations,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    selectConfig: (index) => dispatch(actions.selectConfig(index)),
    changeLines: (lines) => dispatch(actions.changeLines(lines)),
    changeIterations: (value) => dispatch(actions.changeIterations(value)),
    startEditConfig: () => dispatch(startEditConfig()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);

