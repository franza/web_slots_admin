import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import CircularProgress from 'material-ui/CircularProgress';
import { loadConfig } from '../../actions/editConfig';
import { getCurrentConfigId } from '../../reducers';

import './ConfigEditor.css';

class ConfigEditor extends Component {
  constructor(props) {
    super(props);

    this.state = {
      configValue: ''
    };
  }

  componentDidMount() {
    this.props.loadConfig(this.props.currentSlotId);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.config !== nextProps.config) {
      this.setState({ configValue: nextProps.config });

      // for Dialog component properly resizing when children size is changed
      // solution from material-ui Git
      window.dispatchEvent(new Event('resize'));
    }
  }

  getConfigText = () =>{
    return this.state.configValue;
  };

  handleChange = (event) => {
    this.setState({ configValue: event.target.value });
  };

  render() {
    return (
      <div>
        {
          this.props.isConfigLoading ?
            <div className="centered-container">
              <CircularProgress className="loading-spinner"/>
            </div>
            :
            <textarea className="json-area"
                      value={this.state.configValue || ''}
                      onChange={this.handleChange}
            />
        }
      </div>
    );
  }
}

ConfigEditor.propTypes = {
  config: PropTypes.string,
  currentSlotId: PropTypes.string.isRequired,
  loadConfig: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  const { config } = state;
  return {
    config,
    currentSlotId: getCurrentConfigId(state)
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadConfig: (configId) => dispatch(loadConfig(configId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(ConfigEditor);
