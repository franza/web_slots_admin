import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Snackbar, FlatButton, Dialog } from 'material-ui';
import ConfigEditor from '../../components/ConfigEditor/ConfigEditor';
import { stopEditConfig, saveConfig } from '../../actions/editConfig';
import { getCurrentConfigId } from '../../reducers';

class ChangeConfigDialog extends Component {

  constructor(props) {
    super(props);

    this.state = {
      message: '',
    };
  }

  handleSnackbarClose = () => {
    this.setState({
      message: '',
    });
  };

  saveConfig = () => {
    const configText = this.editor.getWrappedInstance().getConfigText();

    try {
      JSON.parse(configText);
    } catch (error) {
      this.setState({ message: 'Error: JSON structure is not valid' });
      return;
    }

    this.props.saveConfig(this.props.currentSlotId, configText);
  };

  closeDialog = () => {
    this.props.stopEditConfig();
  };

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.closeDialog}
        disabled={this.props.isConfigLoading}
      />,
      <FlatButton
        label="Save"
        secondary={true}
        onTouchTap={this.saveConfig}
        disabled={this.props.isConfigLoading}
      />,
    ];

    return (
      <div>
        <Snackbar
          open={this.state.message !== ""}
          message={this.state.message}
          autoHideDuration={2000}
          onRequestClose={this.handleSnackbarClose}
        />
        <Dialog
          title={`Edit config '${this.props.currentSlotId}'`}
          contentStyle={{
            width: "60%",
            maxWidth: "none"
          }}
          autoScrollBodyContent={false}
          actions={actions}
          modal={true}
          open={this.props.isConfigEditing}
        >
          <ConfigEditor ref={(input) => this.editor = input} />
        </Dialog>
      </div>
    );
  }
}

ChangeConfigDialog.propTypes = {
  configsIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  selectedConfig: PropTypes.number.isRequired,
  isConfigLoading: PropTypes.bool.isRequired,
  isConfigEditing: PropTypes.bool.isRequired,
  currentSlotId: PropTypes.string,
  stopEditConfig: PropTypes.func.isRequired,
  saveConfig: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  const { configsIds, selectedConfig, isConfigLoading, isConfigEditing } = state;

  return {
    configsIds,
    selectedConfig,
    isConfigLoading,
    isConfigEditing,
    currentSlotId: getCurrentConfigId(state)
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    stopEditConfig: () => dispatch(stopEditConfig()),
    saveConfig: (configId, configData) => dispatch(saveConfig(configId, configData)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChangeConfigDialog);
